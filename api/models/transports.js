const mongoose = require ('mongoose'); 

const transportSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    transportcode: Number,
    sollwert: Number
});

module.exports = mongoose.model('Transport', transportSchema); 