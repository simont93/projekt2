const mongoose = require ('mongoose'); 

const productSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    name: String,
    price_t: Number,
    silo_nr: Number,
    t_aufLager: Number
});

module.exports = mongoose.model('Product', productSchema); 